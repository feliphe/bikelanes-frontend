import { Component } from '@angular/core';
import { UsuariosDTO } from '../../models/usuarios.dto';
import { UsuariosService } from '../../services/usuarios.service';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MenuController } from 'ionic-angular/components/app/menu-controller';


@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {

  formGroup: FormGroup;
  usuariosDTO: UsuariosDTO;

  constructor(
      public navCtrl: NavController,
      public usuarioService: UsuariosService,
      public formBuilder: FormBuilder,
      public alertCtrl: AlertController,
      public navParams: NavParams, 
      public menu: MenuController) {

        this.formGroup = this.formBuilder.group({
          name: [ 'Joaquim' ,  [Validators.required, Validators.minLength(5), Validators.maxLength(120)] ],
          email: [ 'joaquim@gmail.com' , [Validators.required, Validators.email] ],          
          password : ['123', [Validators.required]],         
        });
  }

  ionViewWillEnter() {
    this.menu.swipeEnable(false);
  }


  signupUser() {
    this.usuarioService
      .insert(this.formGroup.value)
      .subscribe(response => {
        this.showInsertOk();
      },
      error => {
        this.showInsertError();
      }); 
  }

  showInsertOk() {
    let alert = this.alertCtrl.create({
      title: 'Sucesso!',
      message: 'Cadastro efetuado com sucesso',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.navCtrl.pop();
          }
        }
      ]
    });

    alert.present();
  }

  showInsertError() {
    let alert = this.alertCtrl.create({
      title: 'Error!',
      message: 'Email já cadastrado',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Ok'
        }
      ]
    });

    alert.present();
  }
}
