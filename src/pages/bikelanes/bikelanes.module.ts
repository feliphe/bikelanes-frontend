import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BikelanesPage } from './bikelanes';

@NgModule({
  declarations: [
    BikelanesPage,
  ],
  imports: [
    IonicPageModule.forChild(BikelanesPage),
  ],
})
export class BikelanesPageModule {}
