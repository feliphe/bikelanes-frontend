import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { BikelaneService } from '../../services/bikelane.service';
import { BikelaneDTO } from '../../models/bikelane.dto';
import { StorageService } from '../../services/storage.service';
import { UsuariosDTO } from '../../models/usuarios.dto';
import { UsuariosService } from '../../services/usuarios.service';
import { ProfileService } from '../../services/profile.service';


@IonicPage()
@Component({
  selector: 'page-bikelanes',
  templateUrl: 'bikelanes.html',
})
export class BikelanesPage {

  bikelanes: BikelaneDTO[];
  usuario: UsuariosDTO;
  chkFavoritar: boolean = false;

  constructor(
        public navCtrl: NavController
      , public navParams: NavParams
      , public bikelaneService: BikelaneService
      , public storage: StorageService
      , public userService: UsuariosService
      , public profileService: ProfileService
      , public alertCtrl: AlertController
      , public menu: MenuController) {
  }

  ionViewWillEnter() {
    this.menu.swipeEnable(true);
  }

  ionViewDidLoad() {

    this.bikelaneService
        .findAll()
        .subscribe(data => {
          this.bikelanes = data['result']['records'];
        });

    this.userService
        .findByEmail(this.storage.getLocalUser().email)
        .subscribe(user => {
          this.usuario = user;
        });

  }

  favoritar(bikelane: BikelaneDTO) {
    this.profileService
        .insert(bikelane, this.usuario.profile.id)
        .subscribe(response => {
          this.showInsertOk();
        },
        error => {
          this.showInsertError();
        }); ;
  }

  showInsertOk() {
    let alert = this.alertCtrl.create({
      title: 'Sucesso!',
      message: 'Ciclovia Adicionada a sua lista de favoritos.',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Ok'
        }
      ]
    });

    alert.present();
  }

  showInsertError() {
    let alert = this.alertCtrl.create({
      title: 'Atenção!',
      message: 'Essa ciclovia já existe na sua lista de favoritos.',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Ok'
        }
      ]
    });

    alert.present();
  }

}
