import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { UsuariosService } from '../../services/usuarios.service';
import { UsuariosDTO } from '../../models/usuarios.dto';
import { StorageService } from '../../services/storage.service';
import { ProfileService } from '../../services/profile.service';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  user: UsuariosDTO = {email:'', name:'', password:'', profile:null};

  constructor(
        public navCtrl: NavController
      , public navParams: NavParams
      , public userService: UsuariosService
      , public storage: StorageService
      , public profileService: ProfileService
      , public alertCtrl: AlertController) {}

  ionViewDidLoad() {
    this.userService
        .findByEmail(this.storage.getLocalUser().email)
        .subscribe(data => {
          this.user = data;
        });
  }

  alterarNome() {
    this.userService
        .changeName(this.user.id, this.user.name)
        .subscribe(response => {
          this.showInsertOk();
        }, error => {
          this.showInsertError();
        });
  }

  showInsertOk() {
    let alert = this.alertCtrl.create({
      title: 'Sucesso!',
      message: 'Nome alterado.',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Ok'
        }
      ]
    });

    alert.present();
  }

  showInsertError() {
    let alert = this.alertCtrl.create({
      title: 'Atenção!',
      message: 'Ocorreu algum problema, tente novamente.',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Ok'
        }
      ]
    });

    alert.present();
  }
}
