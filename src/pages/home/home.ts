import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { CredenciaisDTO } from '../../models/Credenciais.dto';
import { AuthService } from '../../services/auth.service';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  credenciais: CredenciaisDTO = {
    email: "",
    password: ""
  };

  constructor(
      public navCtrl: NavController
    , public menu: MenuController
    , public auth: AuthService) {

  }

  ionViewWillEnter() {
    this.menu.swipeEnable(false);
  }

  login() {
    this.auth
        .authenticate(this.credenciais)
        .subscribe(response => {
          this.auth.successfullLogin(response.headers.get('Authorization'));
          this.navCtrl.setRoot('BikelanesPage');
        },
        error => {});
  }

  signUp() {
    this.navCtrl.push('SignUpPage');
  }
}
