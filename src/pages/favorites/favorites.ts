import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProfileService } from '../../services/profile.service';
import { UsuariosService } from '../../services/usuarios.service';
import { BikelaneDTO } from '../../models/bikelane.dto';
import { StorageService } from '../../services/storage.service';
import { UsuariosDTO } from '../../models/usuarios.dto';


@IonicPage()
@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html',
})
export class FavoritesPage {

  usuario: UsuariosDTO;
  favorites: BikelaneDTO[];

  constructor(
      public navCtrl: NavController
    , public navParams: NavParams
    , public profileService: ProfileService
    , public userService: UsuariosService
    , public storage: StorageService) {
  }

  ionViewDidLoad() {
    this.userService
        .findByEmail(`${this.storage.getLocalUser().email}`)
        .subscribe(user => {
          this.usuario = user;
          this.profileService
              .findFavorites(user.profile.id)
              .subscribe(list => {
                this.favorites = list;
              });
        });
  }  

  disfavor(bikelane: BikelaneDTO) {
    this.profileService
        .disfavor(this.usuario.profile.id, bikelane._id)
        .subscribe(response => {
          this.favorites = this.favorites
              .filter(e => e._id != bikelane._id);
        });
  }
}
