import { BikelaneDTO } from "./bikelane.dto";

export interface ProfileDTO {
    id: string;
    favorites: BikelaneDTO[];
    recentSearch: BikelaneDTO[];

}
