import { ProfileDTO } from "./profile.dto";

export interface UsuariosDTO {
    id?: string;
    email: string;
    name: string;
    password: string;
    profile: ProfileDTO;
}
