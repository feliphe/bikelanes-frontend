export interface BikelaneDTO {
    _id: string;
    rota: string;
    sentido: string;
    percurso: string;
    bairro: string;
    extensao_km: number;
    inauguracao: string;
    km_acumulada: string;
}