import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BikelaneService } from '../services/bikelane.service';

import { AuthService } from '../services/auth.service';
import { StorageService } from '../services/storage.service';
import { HttpClientModule, HttpClient, HttpClientJsonpModule } from '@angular/common/http';
import { UsuariosService } from '../services/usuarios.service';

import { AuthInterceptorProvider } from '../interceptors/auth-interceptor';
import { AuthInterceptor } from '../interceptors/auth-interceptor';
import { ProfileService } from '../services/profile.service';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientJsonpModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    UsuariosService,
    HttpClient,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BikelaneService,
    AuthService,
    StorageService,
    AuthInterceptorProvider,
    AuthInterceptor,
    ProfileService
  ]
})
export class AppModule {}
