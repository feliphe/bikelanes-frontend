import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthService } from '../services/auth.service';
import { UsuariosService } from '../services/usuarios.service';
import { StorageService } from '../services/storage.service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: string = 'HomePage';

  pages: Array<{title: string, component: string}>;

  constructor(public platform: Platform
            , public statusBar: StatusBar
            , public splashScreen: SplashScreen
            , public auth: AuthService
            , public userService: UsuariosService
            , public storage: StorageService
            , public menu: MenuController) {
    this.initializeApp();

    this.pages = [
      { title: 'Favoritos', component: 'FavoritesPage' },
      { title: 'Ciclofaixas PE', component: 'BikelanesPage' },
      { title: 'Perfil', component: 'ProfilePage' },
      { title: 'Logout', component: '' }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {      
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page: {title: string, component: string}) {
    
    switch (page.title) {
      case 'Logout':
        this.auth.logout();
        this.nav.setRoot('HomePage');
        break;
    
      default:
        this.nav.setRoot(page.component);
        break;
    }

   
  }
}
