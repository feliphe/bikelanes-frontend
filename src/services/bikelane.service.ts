import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { API_CONFIG } from "../config/api.config";
import { Observable } from "rxjs/Observable";

@Injectable()
export class BikelaneService {

    constructor(public http: HttpClient) {}

    findAll() : Observable<Object[]> {
       return this.http.jsonp(`${API_CONFIG.dadosAbertos}`, "callback") as Observable<Object[]>;
    }
    
}