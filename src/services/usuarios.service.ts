import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { API_CONFIG } from "../config/api.config";
import { UsuariosDTO } from "../models/usuarios.dto";
import { Observable } from "rxjs/Observable";

@Injectable()
export class UsuariosService {

    constructor(public http: HttpClient) {

    }

    insert(obj: UsuariosDTO) {
        return this.http.post(
            `${API_CONFIG.baseUrl}/api/users`,
            obj,
            {
                observe: 'response',
                responseType: 'text'
            }
        );
    }

    findByEmail(email: string) : Observable<UsuariosDTO> {
        return this.http.get<UsuariosDTO>(`${API_CONFIG.baseUrl}/api/users/email?value=${email}`);
    }

    update(newUser: UsuariosDTO) {
        return this.http.put(`${API_CONFIG.baseUrl}/api/users/${newUser.id}`, newUser);
    }

    changeName(id: string, newName: string) {
        return this.http.patch(`${API_CONFIG.baseUrl}/api/users/${id}`,
            {
                "id": id,
                "name": newName
            });
    }
}