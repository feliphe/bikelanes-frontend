import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BikelaneDTO } from "../models/bikelane.dto";
import { API_CONFIG } from "../config/api.config";
import { Observable } from "rxjs";

@Injectable()
export class ProfileService {

    constructor(public http: HttpClient) {}

    insert(bikelane: BikelaneDTO, profileId: string) {
        return this.http.post(
            `${API_CONFIG.baseUrl}/api/profiles/${profileId}/favorites`,
            bikelane,
            {
                observe: 'response',
                responseType: 'text'
            }
        );
    }

    findFavorites(profileId: string) : Observable<BikelaneDTO[]> {
        return this.http.get<BikelaneDTO[]>(`${API_CONFIG.baseUrl}/api/profiles/${profileId}/favorites`);
    }

    disfavor(profileId: string, bikelaneId: string) {
        return this.http.delete(`${API_CONFIG.baseUrl}/api/profiles/${profileId}/favorites/${bikelaneId}`);
    }

}